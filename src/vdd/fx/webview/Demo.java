package vdd.fx.webview;

import eu.hansolo.enzo.led.Led;
import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;


/**
 * User: hansolo
 * Date: 02.04.14
 * Time: 16:21
 */
public class Demo extends Application {
    private ScriptEngine        scriptEngine;
    private StackPane           ledContainer;
    private Led                 led;
    private WebView             webView;


    // ******************** Initialization ************************************
    @Override public void init() {
        led = new Led();

        ledContainer = new StackPane(led);
        ledContainer.setPrefSize(400, 400);
        ledContainer.setPadding(new Insets(10, 10, 10, 10));        

        initNashorn();
    }

    private void initNashorn() {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        scriptEngine        = scriptEngineManager.getEngineByName("nashorn");

        // Inject led object into Nashorn engine
        scriptEngine.put("led", led);
    }

    private void initWebView() {
        webView = new WebView();
        webView.setPrefSize(600, 400);
        WebEngine webEngine = webView.getEngine();
        webEngine.load(Demo.class.getResource("editor.html").toExternalForm());
        webEngine.getLoadWorker().stateProperty().addListener((ov, o, n) -> {
            if (Worker.State.SUCCEEDED == n) {
                webEngine.setOnStatusChanged(webEvent -> {
                    try {
                        scriptEngine.eval(webEvent.getData());
                    } catch (Exception e) {/* code was not valid */}
                });
            }
        });
    }


    // ******************** Application start *********************************
    @Override public void start(Stage stage) {
        initWebView();

        HBox pane = new HBox();
        pane.setSpacing(10);
        pane.getChildren().addAll(ledContainer, webView);

        Scene scene = new Scene(pane);

        stage.setTitle("WebView live edit");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
