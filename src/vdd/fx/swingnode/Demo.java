package vdd.fx.swingnode;

import eu.hansolo.steelseries.gauges.Radial;
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.FrameDesign;
import eu.hansolo.steelseries.tools.PointerType;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;


/**
 * User: hansolo
 * Date: 03.04.14
 * Time: 11:00
 */
public class Demo extends Application {
    private HBox      pane; 
    private SwingNode swingNode;
            
    @Override public void init() {
        // Create SwingNode
        swingNode = new SwingNode();

        // Create content of SwingNode
        Radial swingRadial = new Radial();
        swingRadial.setFrameDesign(FrameDesign.BLACK_METAL);
        swingRadial.setBackgroundColor(BackgroundColor.CARBON);
        swingRadial.setPointerType(PointerType.TYPE9);

        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(400, 400));
        panel.add(swingRadial, BorderLayout.CENTER);
        SwingUtilities.invokeLater(() -> swingNode.setContent(panel));

        // Create a JavaFX Slider
        Slider slider = new Slider();
        slider.setOrientation(Orientation.VERTICAL);
        slider.valueProperty().addListener(observable -> 
                                               SwingUtilities.invokeLater(() -> 
                                                                              swingRadial.setValue(slider.getValue())));

        // Combine the SwingNode and the JavaFX Slider in one JavaFX HBox
        pane = new HBox(swingNode, slider);
        pane.setPadding(new Insets(10 ,10, 10 ,10));
        pane.setSpacing(20);    
    }
        
    @Override public void start(Stage stage) {                                                
        StackPane root = new StackPane();
        root.getChildren().addAll(pane);

        Scene scene = new Scene(root);

        stage.setTitle("SwingNode");
        stage.setScene(scene);
        stage.show();        
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
