package vdd.fx.application;

import javafx.geometry.Dimension2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * User: hansolo
 * Date: 02.04.14
 * Time: 15:17
 */
public class WidgetFactory {

    public static final ToolBar createToolBar() {
        ToolBar toolBar = new ToolBar();        
        AnchorPane.setTopAnchor(toolBar, 0d);
        AnchorPane.setLeftAnchor(toolBar, 0d);
        AnchorPane.setRightAnchor(toolBar,0d);
        return toolBar;
    }
    
    public static Button createFirstToolButton(final String ICON_ID, final String TEXT) {
        Button button = createToolButton(ICON_ID, TEXT);
        button.getStyleClass().add("first");
        return button;
    }    
    public static Button createLastToolButton(final String ICON_ID, final String TEXT) {
        Button button = createToolButton(ICON_ID, TEXT);
        button.getStyleClass().add("last");
        return button;
    }    
    public static Button createToolButton(final String ICON_ID, final String TEXT) {
        Region icon = new Region();
        icon.getStyleClass().add("icon");
        icon.setId(ICON_ID);

        Button button = new Button(TEXT);
        button.setContentDisplay(ContentDisplay.LEFT);
        button.setGraphic(icon);

        return button;
    }

    public static ToggleButton createFirstToolToggleButton(final String ICON_ID, final String TEXT) {
        ToggleButton button = createToolToggleButton(ICON_ID, TEXT);
        button.getStyleClass().add("first");
        return button;
    }
    public static ToggleButton createLastToolToggleButton(final String ICON_ID, final String TEXT) {
        ToggleButton button = createToolToggleButton(ICON_ID, TEXT);
        button.getStyleClass().add("last");
        return button;
    }
    public static ToggleButton createToolToggleButton(final String ICON_ID, final String TEXT) {
        Region icon = new Region();
        icon.getStyleClass().add("icon");
        icon.setId(ICON_ID);

        ToggleButton button = new ToggleButton(TEXT);
        button.setContentDisplay(ContentDisplay.LEFT);
        button.setGraphic(icon);

        return button;
    }
    
    
    public static Stage createDialogStage(final String TITLE, final Parent CONTENT, final String STYLE_SHEET, 
                                          final boolean RESIZABLE) {
        return createDialogStage(TITLE, CONTENT, STYLE_SHEET, new Dimension2D(0,0), RESIZABLE);
    }
    
    public static Stage createDialogStage(final String TITLE, final Parent CONTENT, 
                                          final String STYLE_SHEET, final Dimension2D SIZE, final boolean RESIZABLE) {
        
        Scene scene = SIZE.getWidth() == 0 && SIZE.getHeight() == 0 ? 
                      new Scene(CONTENT) : 
                      new Scene(CONTENT, SIZE.getWidth(), SIZE.getHeight());
        CONTENT.getStylesheets().add(WidgetFactory.class.getResource(STYLE_SHEET).toExternalForm());
        
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(TITLE);
        stage.setScene(scene);
        stage.setResizable(RESIZABLE);
        
        return stage;
    }
}
