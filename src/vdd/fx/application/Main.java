package vdd.fx.application;

import eu.hansolo.enzo.led.Led;
import eu.hansolo.steelseries.gauges.Radial;
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.FrameDesign;
import eu.hansolo.steelseries.tools.PointerType;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import vdd.fx.extendingcontrols.SearchTextField;
import vdd.fx.fxml.CalculatorController;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.net.URL;


/**
 * User: hansolo
 * Date: 02.04.14
 * Time: 15:15
 */
public class Main extends Application {
    private static final Dimension2D SIZE = new Dimension2D(720, 480);

    // ToolBar
    private ToggleButton calculatorButton;
    private Button       editorButton;
    private Button       swingButton;
    private Button       exitButton;
    private ToolBar      toolBar;

    // Calculator
    private Parent       calculator;
    private Stage        calculatorStage;

    // Live editor
    private HBox         liveEditorPane;

    // Swing
    private HBox         swingPane;


    // ******************** Initialization ************************************
    @Override public void init() {
        // Create the toolbar buttons        
        calculatorButton = WidgetFactory.createFirstToolToggleButton("calculator", "Calculator");
        editorButton     = WidgetFactory.createToolButton("editor", "Editor");
        swingButton      = WidgetFactory.createToolButton("gauge", "Swing");
        exitButton       = WidgetFactory.createLastToolButton("exit", "Exit");

        // Create the segmented button bar
        HBox segmentedButtonBar = new HBox();
        segmentedButtonBar.getStyleClass().add("segmented-button-bar");
        segmentedButtonBar.getChildren().addAll(calculatorButton, editorButton, swingButton, exitButton);

        // Create SearchField and Spacer
        SearchTextField searchTextField = new SearchTextField();

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        // Create the toolbar
        toolBar = WidgetFactory.createToolBar();
        toolBar.getItems().addAll(segmentedButtonBar, spacer, searchTextField);

        // Add the listeners
        registerListeners();
    }

    private void initCalculator() {
        // Init calculator
        try {
            calculator = FXMLLoader.load(new URL(CalculatorController.class.getResource("calculator.fxml").toExternalForm()));
        } catch (IOException e) {
            throw new RuntimeException("Error loading fxml file");
        }
    }
    
    private void initSwing() {        
        // Create SwingNode
        SwingNode swingNode = new SwingNode();        

        // Create content of SwingNode
        Radial swingRadial = new Radial();
        swingRadial.setTitle("Java Swing");
        swingRadial.setUnitString("SteelSeries");
        swingRadial.setFrameDesign(FrameDesign.BLACK_METAL);
        swingRadial.setBackgroundColor(BackgroundColor.CARBON);
        swingRadial.setPointerType(PointerType.TYPE9);

        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(400, 400));
        panel.setBackground(new java.awt.Color(216, 222, 227));
        panel.add(swingRadial, BorderLayout.CENTER);
        SwingUtilities.invokeLater(() -> swingNode.setContent(panel));

        // Create a JavaFX Slider
        Slider slider = new Slider();
        slider.setOrientation(Orientation.VERTICAL);
        slider.valueProperty().addListener(observable ->
                                               SwingUtilities.invokeLater(() ->
                                                                              swingRadial.setValue(slider.getValue())));

        swingPane = new HBox(swingNode, slider);
        swingPane.setPadding(new Insets(10, 10, 10, 10));
        swingPane.setSpacing(40);
        swingPane.setBackground(new Background(new BackgroundFill(Color.rgb(216, 222, 227),
                                                                  CornerRadii.EMPTY,
                                                                  Insets.EMPTY)));
        swingPane.setPrefSize(600, 400);
        swingPane.setAlignment(Pos.CENTER);
        AnchorPane.setTopAnchor(swingPane, 55d);
        AnchorPane.setRightAnchor(swingPane, 0d);
        AnchorPane.setBottomAnchor(swingPane, 0d);
        AnchorPane.setLeftAnchor(swingPane, 0d);
    }
    
    private void initLiveEditor() {
        // Create an Enzo Led control
        Led led = new Led();

        StackPane ledContainer = new StackPane(led);
        ledContainer.setPrefSize(200, 200);
        ledContainer.setPadding(new Insets(10, 10, 10, 10));

        // Create a Nashorn script engine instance
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine        scriptEngine        = scriptEngineManager.getEngineByName("nashorn");

        // Inject led object into Nashorn to be able to modify the led object from JavaScript
        scriptEngine.put("led", led);

        // Initialize WebView (has to be done on the FXApplicationThread)
        WebView webView = new WebView();
        webView.setPrefSize(400, 200);
        HBox.setHgrow(webView, Priority.ALWAYS);
        
        WebEngine webEngine = webView.getEngine();
        webEngine.load(Main.class.getResource("editor.html").toExternalForm());
        webEngine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
            if (Worker.State.SUCCEEDED == newState) {
                webEngine.setOnStatusChanged(stringWebEvent -> {
                    try {
                        scriptEngine.eval(stringWebEvent.getData());
                    } catch (Exception e) {}
                });
            }
        });        
        
        // Setup the Live editor pane
        liveEditorPane = new HBox();
        liveEditorPane.setSpacing(10);
        liveEditorPane.getChildren().addAll(ledContainer, webView);
        AnchorPane.setTopAnchor(liveEditorPane, 55d);
        AnchorPane.setRightAnchor(liveEditorPane, 0d);
        AnchorPane.setBottomAnchor(liveEditorPane, 0d);
        AnchorPane.setLeftAnchor(liveEditorPane, 0d);
        
        unmanageNode(liveEditorPane);
    }
    
    
    // ******************** Hookup listeners **********************************
    private void registerListeners() {
        calculatorButton.setOnAction(this::handleActionEvent);
        editorButton.setOnAction(this::handleActionEvent);
        swingButton.setOnAction(this::handleActionEvent);
        exitButton.setOnAction(this::handleActionEvent);
    }


    // ******************** Event handling ************************************
    private void handleActionEvent(ActionEvent event) {
        final Object SRC = event.getSource();
        if (SRC.equals(calculatorButton)) {
            if (null == calculatorStage) { 
                // Create the DialogStage
                calculatorStage = WidgetFactory.createDialogStage("Calculator", 
                                                                  calculator, 
                                                                  "/vdd/fx/fxml/calculator-alternative.css", 
                                                                  new Dimension2D(400, 280),
                                                                  false); 
                
                // Deselect Calculator ToggleButton when close
                calculatorStage.setOnHiding(evt -> calculatorButton.setSelected(false));
            }
            calculatorStage.show();
        } else if (SRC.equals(editorButton)) {            
            unmanageNode(swingPane);
            manageNode(liveEditorPane);            
        } else if (SRC.equals(swingButton)) {
            unmanageNode(liveEditorPane);
            manageNode(swingPane);
        } else if (SRC.equals(exitButton)) {
            Platform.exit();
        }
    }

    
    // ******************** Utility methods ***********************************
    private static final void manageNode(final Node NODE) {
        NODE.setManaged(true);
        NODE.setVisible(true);
    }
    private static final void unmanageNode(final Node NODE) {
        NODE.setVisible(false);
        NODE.setManaged(false);
    }

    
    // ******************** Application related *******************************
    @Override public void start(Stage stage) {
        initCalculator();
        initSwing();
        initLiveEditor();
        
        AnchorPane pane = new AnchorPane();        
        pane.setId("background");
        pane.getChildren().addAll(toolBar, liveEditorPane, swingPane);

        Scene scene = new Scene(pane, SIZE.getWidth(), SIZE.getHeight());
        scene.getStylesheets().add(Main.class.getResource("styles.css").toExternalForm());

        stage.setTitle("VDD Demo");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
