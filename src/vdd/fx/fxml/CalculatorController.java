package vdd.fx.fxml;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

import java.net.URL;
import java.util.ResourceBundle;


public class CalculatorController implements Initializable {
    public enum Operation {
        NONE     { public double eval(double x, double y) {return 0;    } },
        PLUS     { public double eval(double x, double y) {return x + y;} },
        MINUS    { public double eval(double x, double y) {return x - y;} },
        MULTIPLY { public double eval(double x, double y) {return x * y;} },
        DIVIDE   { public double eval(double x, double y) {return x / y;} };

        public abstract double eval(double x, double y);
    }

    private StringProperty   operator1        = new SimpleStringProperty("0");
    private StringProperty   operator2        = new SimpleStringProperty("0");
    private StringProperty   lcdValue         = new SimpleStringProperty("0");
    private StringBuilder    currentValue     = new StringBuilder(16);
    private int              currentOperator  = 1;
    private Operation        operation        = Operation.NONE;   
    private Clipboard        clipboard;
    private ClipboardContent clipboardContent;

    @FXML private Button zero;
    @FXML private Button one;
    @FXML private Button two;
    @FXML private Button three;
    @FXML private Button four;
    @FXML private Button five;
    @FXML private Button six;
    @FXML private Button seven;
    @FXML private Button eight;
    @FXML private Button nine;
    @FXML private Button dot;
    @FXML private Button plusminus;
    @FXML private Button equal;
    @FXML private Button plus;
    @FXML private Button minus;
    @FXML private Button multiply;
    @FXML private Button divide;
    @FXML private Button ac;
    @FXML private Button c;
    @FXML private Button del;
    @FXML private Label  lcd;


    // ******************** Initialization ************************************
    @Override public void initialize(URL url, ResourceBundle rb) {
        lcd.textProperty().bind(lcdValue);
    }


    // ******************** Private Methods ***********************************
    @FXML private void handleButtonAction(ActionEvent event) {
        final Object SOURCE = event.getSource();
        if (SOURCE.equals(one)) {
            currentValue.append("1");
            updateLcdValue();
        } else if (SOURCE.equals(two)) {
            currentValue.append("2");
            updateLcdValue();
        } else if (SOURCE.equals(three)) {
            currentValue.append("3");
            updateLcdValue();
        } else if (SOURCE.equals(four)) {
            currentValue.append("4");
            updateLcdValue();
        } else if (SOURCE.equals(five)) {
            currentValue.append("5");
            updateLcdValue();
        } else if (SOURCE.equals(six)) {
            currentValue.append("6");
            updateLcdValue();
        } else if (SOURCE.equals(seven)) {
            currentValue.append("7");
            updateLcdValue();
        } else if (SOURCE.equals(eight)) {
            currentValue.append("8");
            updateLcdValue();
        } else if (SOURCE.equals(nine)) {
            currentValue.append("9");
            updateLcdValue();
        } else if (SOURCE.equals(zero)) {
            currentValue.append("0");
            updateLcdValue();
        } else if (SOURCE.equals(dot)) {
            if (!lcdValue.toString().contains(".")) {
                currentValue.append(".");
            }
            updateLcdValue();
        } else if (SOURCE.equals(plusminus)) {
            if (currentValue.length() > 0) {
                if (currentValue.charAt(0) == 45) {
                    currentValue.delete(0, 1);
                } else {
                    currentValue.insert(0, "-");
                }
            }
            updateLcdValue();
        } else if (SOURCE.equals(plus)) {
            doOperation();
            operation = Operation.PLUS;
            updateOperator();
        } else if (SOURCE.equals(minus)) {
            doOperation();
            operation = Operation.MINUS;
            updateOperator();
        } else if (SOURCE.equals(multiply)) {
            doOperation();
            operation = Operation.MULTIPLY;
            updateOperator();
        } else if (SOURCE.equals(divide)) {
            doOperation();
            operation = Operation.DIVIDE;
            updateOperator();
        } else if (SOURCE.equals(ac)) {
            reset();
        } else if (SOURCE.equals(c)) {
            resetLast();
        } else if (SOURCE.equals(del)) {
            int length = currentValue.length();
            if (length >= 1) {
                if (length == 1) {
                    currentValue.setLength(0);
                    currentValue.append("0");
                } else {
                    currentValue.replace(length - 1, length, "");
                }
                updateLcdValue();
            }
        } else if (SOURCE.equals(equal)) {
            doOperation();
        }
    }

    private void updateLcdValue() {
        if (currentOperator == 1) {
            operator1.set(currentValue.toString());
        } else if (currentOperator == 2) {
            operator2.set(currentValue.toString());
        }
        lcdValue.set(currentValue.toString());
        equal.requestFocus();
    }

    private void updateOperator() {
        if (currentOperator == 1) {
            currentOperator = 2;
        } else if (currentOperator == 2) {
            String result = Double.toString(operation.eval(Double.parseDouble(operator1.get()), Double.parseDouble(operator2.get())));
            lcdValue.set(result);
            operator1.set(result);
            currentOperator = 2;
        }
        currentValue.setLength(0);
    }

    private void doOperation() {
        if (null == clipboard && Platform.isFxApplicationThread()) {
            clipboard        = Clipboard.getSystemClipboard();
            clipboardContent = new ClipboardContent();    
        }
        
        if (operation != Operation.NONE && !operator1.get().isEmpty() && !operator2.get().isEmpty()) {
            String result = Double.toString(operation.eval(Double.parseDouble(operator1.get()), Double.parseDouble(operator2.get())));
            lcdValue.set(result);
            operator1.set(result);
            currentOperator = 1;
            operation = Operation.NONE;
            currentValue.setLength(0);
            currentValue.append(result);
            // Put result to System clipboard
            clipboardContent.clear();
            clipboardContent.putString(result);
            clipboard.setContent(clipboardContent);
        }
    }

    private void reset() {
        operation = Operation.NONE;
        currentOperator = 1;
        currentValue.setLength(0);
        operator1.set("0");
        operator2.set("0");
        lcdValue.set("0");
    }

    private void resetLast() {
        if (currentOperator == 1) {
            reset();
        } else if (currentOperator == 2) {
            operator2.set("0");
            currentValue.setLength(0);
        }
    }
}
