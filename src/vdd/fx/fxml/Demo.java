package vdd.fx.fxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;


public class Demo extends Application {

    @Override public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(new URL(getClass().getResource("/vdd/fx/fxml/calculator.fxml").toExternalForm())) ;

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("CalculatorFX");
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
