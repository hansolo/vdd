package vdd.fx.extendingcontrols;

import javafx.application.Platform;
import javafx.scene.control.TextField;


/**
 * Created by
 * User: hansolo
 * Date: 30.08.13
 * Time: 15:27
 */
public class SearchTextField extends TextField {

    public SearchTextField(){
        super();        
        getStylesheets().add(SearchTextField.class.getResource("searchtextfield.css").toExternalForm());
        if (Platform.isFxApplicationThread()) {
            setSkin(new SearchTextFieldSkin(this));
        } else {
            Platform.runLater(() -> setSkin(new SearchTextFieldSkin(this)));
        }        
    }
}
