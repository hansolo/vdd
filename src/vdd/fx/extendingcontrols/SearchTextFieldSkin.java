package vdd.fx.extendingcontrols;

import com.sun.javafx.scene.control.skin.TextFieldSkin;
import javafx.scene.layout.Region;


/**
 * Created by
 * User: hansolo
 * Date: 30.08.13
 * Time: 15:27
 */
public class SearchTextFieldSkin extends TextFieldSkin {
    private static final double ICON_SIZE = 20;
    private Region loupe;
    private Region crossButton;

    public SearchTextFieldSkin(final SearchTextField CONTROL){
        super(CONTROL);
        
        initGraphics();
        registerListeners();
    }

    private void initGraphics() {
        loupe = new Region();
        loupe.getStyleClass().add("loupe");
        loupe.setFocusTraversable(false);

        crossButton = new Region();
        crossButton.getStyleClass().add("cross-button");
        crossButton.setFocusTraversable(false);
        crossButton.setVisible(false);

        getChildren().addAll(loupe, crossButton);
    }

    private void registerListeners() {                
        getSkinnable().textProperty().addListener(observable ->
            crossButton.setVisible(!getSkinnable().getText().isEmpty())
        );
                
        getSkinnable().focusedProperty().addListener(observable -> {
            loupe.setVisible(!getSkinnable().isFocused() && getSkinnable().getText().isEmpty());
            crossButton.setVisible(getSkinnable().isFocused() && !getSkinnable().getText().isEmpty());
        });
                
        getSkinnable().widthProperty().addListener(observable -> {                        
            loupe.setTranslateX(-getSkinnable().getWidth() * 0.5 + ICON_SIZE * 0.7);
            crossButton.setTranslateX(getSkinnable().getWidth() * 0.5 - ICON_SIZE * 0.7);
        });
                       
        crossButton.setOnMouseClicked(event -> getSkinnable().setText(""));
    }
}
